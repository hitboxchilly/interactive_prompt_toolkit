import sys
from setuptools import setup

requires = ['pygments', 'prompt_toolkit']

if sys.platform == 'win32':
    requires.append('pywin32')

setup(name='interactive_prompt_toolkit',
      version = '0.1',
      description = 'Hook up prompt_toolkit and pygments to give the interactive'
                  ' interpreter session syntax highlighting and readline'
                  ' advantages.',
      url = '',
      license = 'MIT',
      author = 'Carl Harris',
      author_email = 'elgoogemail2007@gmail.com',
      py_modules = ['interactive_prompt_toolkit'],
      install_requires = requires,
      zip_safe = False)
