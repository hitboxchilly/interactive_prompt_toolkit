import argparse
import sys

import ctypes
import win32api

from prompt_toolkit import PromptSession
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
from prompt_toolkit.history import InMemoryHistory
from prompt_toolkit.lexers import PygmentsLexer
from prompt_toolkit.styles import merge_styles, style_from_pygments_cls
from pygments.lexers.python import PythonLexer
from pygments.styles import get_all_styles, get_style_by_name

READLINE = ctypes.CFUNCTYPE(ctypes.c_char_p, ctypes.c_void_p, ctypes.c_void_p, ctypes.c_char_p)

class _Readline:
    """
    Keep references alive.
    """

    def __init__(self, hook=None, reference=None):
        self.hook = hook
        self.reference = reference


_readline = _Readline()

_strncpy = ctypes.windll.kernel32.lstrcpynA
_strncpy.restype = ctypes.c_char_p
_strncpy.argtypes = [ctypes.c_char_p, ctypes.c_char_p, ctypes.c_size_t]

ctypes.pythonapi.PyMem_RawMalloc.restype = ctypes.c_size_t
ctypes.pythonapi.PyMem_RawMalloc.argtypes = [ctypes.c_size_t]

_encoding = sys.getdefaultencoding()

style = style_from_pygments_cls(get_style_by_name('paraiso-dark'))

promptsession = PromptSession(
    lexer = PygmentsLexer(PythonLexer),
    auto_suggest = AutoSuggestFromHistory(),
    style = style,
)

def readline(stdin, stdout, prompt):
    try:
        res = promptsession.prompt(prompt.decode(_encoding))
    except KeyboardInterrupt:
        return 0
    except EOFError:
        res = ''
    else:
        res = res + '\n'
    res = res.encode(_encoding)
    n = len(res) + 1
    dest = ctypes.pythonapi.PyMem_RawMalloc(n)
    _strncpy(ctypes.cast(dest, ctypes.c_char_p), ctypes.cast(res, ctypes.c_char_p), n)
    return dest

def install(hook):
    _readline.hook = hook
    addr = win32api.GetProcAddress(sys.dllhandle, "PyOS_ReadlineFunctionPointer")
    PyOS_ReadlineFunctionPointer = ctypes.c_void_p.from_address(addr)
    readline_func = READLINE(readline)
    _readline.reference = readline_func
    PyOS_ReadlineFunctionPointer.value = ctypes.c_void_p.from_address(ctypes.addressof(readline_func)).value

def main(argv=None):
    """
    """
    parser = argparse.ArgumentParser(prog='interactive_prompt_toolkit', description=main.__doc__)
    local = globals()
    local.update(locals())
    import code
    code.interact(local=local, readfunc=promptsession.prompt)
    return
    #install(readline)

if __name__ == '__main__':
    main()
